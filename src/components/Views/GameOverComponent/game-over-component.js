export default {

    name: 'game-over-component',

    props: [],

    data() {
        return {

        }
    },

    methods: {

        restartGame() {
            this.$emit( 'restartGame' );
        },

    },

    components: {

    },

    computed: {},

    mounted() {
    },
}