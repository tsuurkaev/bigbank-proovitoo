export default {

    name: 'stats-component',

    props: ['gameInfo','reputation'],

    data() {
        return {

            singleMessage: {},

        }
    },

    methods: {

        round(value, decimals) {
            return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        },

    },

    components: {

    },

    computed: {},

    mounted() {
    },
}