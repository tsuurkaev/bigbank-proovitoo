export default {

    name: 'message-board-component',

    props: ['messages','sortedMessages','singleMessage'],

    data() {
        return {

        }
    },

    methods: {

        takeQuest(adId) {
            this.$emit( 'takeQuest', adId );
        },

        getDangerLevel(probability) {
            switch(probability) {
                case "Sure thing":
                    return "gray";
                case "Piece of cake":
                    return "gray";

                case "Walk in the park":
                    return "green";
                case "Quite likely":
                    return "green";

                case "Hmmm....":
                    return "yellow";
                case "Rather detrimental":
                    return "yellow";

                case "Gamble":
                    return "orange";
                case "Risky":
                    return "orange";
                case "Playing with fire":
                    return "orange";

                case "Suicide mission":
                    return "red";
                case "Impossible":
                    return "red";

                default:
                    return "purple";
            }
        },

        showMessage(adId) {
            this.$emit( 'showMessage', adId );
        },

        hideMessage() {
            this.$emit( 'hideMessage');
        },

    },

    components: {

    },

    computed: {},

    mounted() {
    },
}