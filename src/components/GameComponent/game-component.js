import HeaderComponent from '@/components/Layout/HeaderComponent/HeaderComponent.vue'
import FooterComponent from '@/components/Layout/FooterComponent/FooterComponent.vue'

import StatsComponent from '@/components/Helpers/StatsComponent/StatsComponent.vue'

import GameOverComponent from '@/components/Views/GameOverComponent/GameOverComponent.vue'
import ShopComponent from '@/components/Views/ShopComponent/ShopComponent.vue'
import MessageBoardComponent from '@/components/Views/MessageBoardComponent/MessageBoardComponent.vue'

import { RadialMenu,  RadialMenuItem } from 'vue-radial-menu'

export default {

    name: 'game-component',

    props: [],

    data() {
        return {

            baseUrl: 'https://dragonsofmugloar.com/api/v2',

            gameInfo: {
                gameId: "",
                gold: 0,
                highScore: 0,
                level: 0,
                lives: 0,
                score: 0,
                turn: 0,
            },
            reputation: {},
            messages: {},
            singleMessage: {},

            sortedMessages: {
                steal:[],
                escort:[],
                create:[],
                help:[],
                other:[]
            },

            shopItems: {},

            activePage: "quest",

            infoMessageArray: [],

            showModal: false,

            menuItems: ['shop', 'quest', 'nature', 'reset'],

            gameOver: false

        }
    },

    methods: {

        startNewGame() {
            this.axios({
                method: 'POST',
                url: this.baseUrl+'/game/start'
            }).then((response) => {
                this.gameInfo = response.data;
                this.updateGameInfo();
            });
        },

        restartGame() {
            this.startNewGame();
            this.gameOver = false;
            this.changePage('quest');
        },

        updateGameInfo() {
            this.investigateReputation();
            this.getMessages();
            this.getShop();
            localStorage.setItem("gameInfo", JSON.stringify(this.gameInfo));
        },

        getShop() {
            let gameId = this.gameInfo['gameId'];
            this.axios({
                method: 'GET',
                url: this.baseUrl+'/'+gameId+'/shop'
            }).then((response) => {
                this.shopItems = response.data;
            });
        },

        investigateReputation() {
            let gameId = this.gameInfo['gameId'];
            this.axios({
                method: 'POST',
                url: this.baseUrl+'/'+gameId+'/investigate/reputation'
            }).then((response) => {
                this.reputation = response.data;
            });
        },

        getMessages() {
            let gameId = this.gameInfo['gameId'];
            this.axios({
                method: 'GET',
                url: this.baseUrl+'/'+gameId+'/messages'
            }).then((response) => {
                this.messages = response.data;
                this.messages.sort(function(a, b) {
                    return  b.reward - a.reward;
                });
                this.checkForEncryption()
                this.sortMessageToCategories();
            });
        },

        checkForEncryption() {
            for (let key in this.messages) {
                switch (this.messages[key].encrypted) {
                    case 1:
                        // base64 decode
                        this.messages[key].adId = atob(this.messages[key].adId);
                        this.messages[key].message = atob(this.messages[key].message);
                        this.messages[key].probability = atob(this.messages[key].probability);
                        break;
                    case 2: {
                        // vigenere-cipher
                        this.messages[key].adId = this.decrypt(this.messages[key].adId, 'N');
                        this.messages[key].message = this.decrypt(this.messages[key].message, 'N');
                        this.messages[key].probability = this.decrypt(this.messages[key].probability, 'N');
                        break;
                    }
                    default:
                    // nothing
                }
            }
        },

        sortMessageToCategories() {
            let sortedMessages = {};
            let encryptedMessages = [];
            for (let key in this.messages) {
                let message = this.messages[key];
                let str = message.message;
                let res = str.split(" ");

                if (res.length > 1){
                    if (sortedMessages[res[0]] == undefined) {
                        sortedMessages[res[0]] = []
                    } else {
                        sortedMessages[res[0]].push(message);
                    }
                } else {
                    encryptedMessages.push(message)
                }

            }
            sortedMessages["other"] = encryptedMessages;

            this.sortedMessages = sortedMessages;
        },

        takeQuest(adId) {
            let index = this.messages.findIndex((e) => e.adId === adId);
            this.messages.splice( index, 1 );
            this.sortMessageToCategories();

            let gameId = this.gameInfo['gameId'];
            this.axios({
                method: 'POST',
                url: this.baseUrl+'/'+gameId+'/solve/'+adId
            }).then((response) => {
                if (response.data.message == 'You were defeated on your last mission!') {
                    this.gameOver = true;
                    this.changePage('gameOver')
                } else {
                    this.gameInfo.lives = response.data.lives;
                    this.gameInfo.gold = response.data.gold;
                    this.gameInfo.score = response.data.score;
                    this.gameInfo.highScore = response.data.highScore;
                    this.gameInfo.turn = response.data.turn;
                    this.showInfoMessage(response.data.message);
                    this.singleMessage = {};
                    this.updateGameInfo();
                }
            });
        },

        showMessage(adId) {
            let index = this.messages.findIndex((e) => e.adId === adId);
            this.singleMessage = this.messages[index];
        },

        hideMessage() {
            this.singleMessage = {};
        },

        buyItem(data) {
            if (!data.disabled) {
                let gameId = this.gameInfo['gameId'];
                this.axios({
                    method: 'POST',
                    url: this.baseUrl+'/'+gameId+'/shop/buy/'+data.id
                }).then((response) => {
                    if (response.data.status == 'Game Over') {
                        this.gameOver = true;
                        this.changePage('gameOver')
                    } else {
                        this.gameInfo.lives = response.data.lives;
                        this.gameInfo.gold = response.data.gold;
                        this.gameInfo.level = response.data.level;
                        this.gameInfo.turn = response.data.turn;
                        this.showInfoMessage("Level up");
                        this.updateGameInfo();
                    }
                });
            } else {
                this.showInfoMessage("This item is too expensive");
            }

        },

        showInfoMessage (infoMessage) {
            this.infoMessageArray.push(infoMessage);
            setTimeout(this.removeLastInfoMessageFromArray, 3000);
        },

        removeLastInfoMessageFromArray () {
            this.infoMessageArray.splice(0, 1)
        },

        changePage(page) {
            if (page == 'reset'){
                this.restartGame();
            } else if(this.gameOver){
                this.gameInfo.lives = 0;
                this.activePage = 'gameOver';
            } else {
                this.activePage = page;
            }
        },

        /*
         * Vigenère cipher
         *
         * Copyright (c) 2017 Project Nayuki
         * All rights reserved. Contact Nayuki for licensing.
         * https://www.nayuki.io/page/vigenere-cipher-javascript
         *
         * Did not contact anyone, just needed it to decipher some text, sorry, T.S
         */
        decrypt(cipherText){
            // for some reason it is just N, just a letter N, i dont know why, i will ask you this
            let key = this.filterKey("N");
            for (let i = 0; i < key.length; i++) {
                key[i] = (26 - key[i]) % 26;
            }
            let cypheredValue = this.crypt(cipherText, key);
            return cypheredValue;
        },

        crypt(input, key) {
            let output = "";
            for (let i = 0, j = 0; i < input.length; i++) {
                let c = input.charCodeAt(i);
                if (this.isUppercase(c)) {
                    output += String.fromCharCode((c - 65 + key[j % key.length]) % 26 + 65);
                    j++;
                } else if (this.isLowercase(c)) {
                    output += String.fromCharCode((c - 97 + key[j % key.length]) % 26 + 97);
                    j++;
                } else {
                    output += input.charAt(i);
                }
            }
            return output;
        },

        filterKey(key) {
            let result = [];
            for (let i = 0; i < key.length; i++) {
                let c = key.charCodeAt(i);
                if (this.isLetter(c))
                    result.push((c - 65) % 32);
            }
            return result;
        },

        isLetter(c) {
            return this.isUppercase(c) || this.isLowercase(c);
        },

        isUppercase(c) {
            return 65 <= c && c <= 90;  // 65 is character code for 'A'. 90 is 'Z'.
        },

        isLowercase(c) {
            return 97 <= c && c <= 122;  // 97 is character code for 'a'. 122 is 'z'.
        },

        /*
         *  end of Vigenère cipher
         */

    },

    components: {
        HeaderComponent,
        FooterComponent,
        ShopComponent,
        StatsComponent,
        GameOverComponent,
        MessageBoardComponent,
        RadialMenu,
        RadialMenuItem
    },

    computed: {},

    mounted() {
        if (localStorage.getItem("gameInfo") === null) {
            this.startNewGame();
        } else {
            this.gameInfo = JSON.parse(localStorage.getItem("gameInfo"));
            this.updateGameInfo();
        }

    },
}