import { RadialMenu,  RadialMenuItem } from 'vue-radial-menu'

export default {

    name: 'footer-component',

    props: ['infoMessageArray'],

    data() {
        return {

            singleMessage: {},
            menuItems: ['shop', 'quest', 'nature', 'reset'],

        }
    },

    methods: {
        changePage(page) {
            this.$emit( 'changePage', page );
        }
    },

    components: {
        RadialMenu,
        RadialMenuItem
    },

    computed: {},

    mounted() {
    },
}