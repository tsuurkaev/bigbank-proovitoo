export default {

    name: 'shop-component',

    props: ['shopItems', 'gameInfo'],

    data() {
        return {

        }
    },

    methods: {

        buyItem(id, disabled) {
            this.$emit( 'buyItem', {'id':id, 'disabled':disabled} );
        },

    },

    components: {

    },

    computed: {},

    mounted() {
    },
}