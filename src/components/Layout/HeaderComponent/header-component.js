import { Slide } from 'vue-burger-menu'
import StatsComponent from '@/components/Helpers/StatsComponent/StatsComponent.vue'

export default {

    name: 'header-component',

    props: ['gameInfo','reputation'],

    data() {
        return {

            singleMessage: {},

        }
    },

    methods: {

        round(value, decimals) {
            return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        },

    },

    components: {
        Slide,
        StatsComponent
    },

    computed: {},

    mounted() {
    },
}